package com.chenyong.demo.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 社会化用户 前端控制器
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Controller
@RequestMapping("/demo/socialuser")
public class SocialuserController {

}

