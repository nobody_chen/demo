package com.chenyong.demo.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author chen
 * @date 2018/4/28 17:15
 */
@Controller
@RequestMapping("/")
public class IndexController {

    @GetMapping
    public String index(){
        return "shop/index";
    }

}
