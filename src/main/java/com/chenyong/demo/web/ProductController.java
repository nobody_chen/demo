package com.chenyong.demo.web;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.chenyong.demo.entity.Product;
import com.chenyong.demo.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 商品 前端控制器
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Controller
@RequestMapping("/demo/product")
public class ProductController {

    @Autowired
    private IProductService iProductService;

    @RequestMapping("/list")
    @ResponseBody
    public Page<Product> list(String id){
        return iProductService.selectPage(new Page<Product>(1,10),new EntityWrapper<Product>().gt("marketPrice",20));
    }


}

