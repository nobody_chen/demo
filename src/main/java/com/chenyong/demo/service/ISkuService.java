package com.chenyong.demo.service;

import com.chenyong.demo.entity.Sku;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * SKU 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ISkuService extends IService<Sku> {

}
