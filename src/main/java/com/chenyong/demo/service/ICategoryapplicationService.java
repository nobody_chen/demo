package com.chenyong.demo.service;

import com.chenyong.demo.entity.Categoryapplication;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 经营分类申请 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ICategoryapplicationService extends IService<Categoryapplication> {

}
