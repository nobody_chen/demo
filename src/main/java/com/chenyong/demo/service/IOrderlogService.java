package com.chenyong.demo.service;

import com.chenyong.demo.entity.Orderlog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单记录 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrderlogService extends IService<Orderlog> {

}
