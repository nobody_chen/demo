package com.chenyong.demo.service;

import com.chenyong.demo.entity.Memberdepositlog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员预存款记录 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IMemberdepositlogService extends IService<Memberdepositlog> {

}
