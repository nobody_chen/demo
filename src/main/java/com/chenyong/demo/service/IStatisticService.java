package com.chenyong.demo.service;

import com.chenyong.demo.entity.Statistic;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 统计 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IStatisticService extends IService<Statistic> {

}
