package com.chenyong.demo.service;

import com.chenyong.demo.entity.Storerank;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 店铺等级 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IStorerankService extends IService<Storerank> {

}
