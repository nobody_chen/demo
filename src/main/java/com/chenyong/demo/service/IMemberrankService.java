package com.chenyong.demo.service;

import com.chenyong.demo.entity.Memberrank;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员等级 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IMemberrankService extends IService<Memberrank> {

}
