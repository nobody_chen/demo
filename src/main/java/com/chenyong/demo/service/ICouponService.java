package com.chenyong.demo.service;

import com.chenyong.demo.entity.Coupon;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优惠券 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ICouponService extends IService<Coupon> {

}
