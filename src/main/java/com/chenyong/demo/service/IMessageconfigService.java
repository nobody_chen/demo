package com.chenyong.demo.service;

import com.chenyong.demo.entity.Messageconfig;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 消息配置 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IMessageconfigService extends IService<Messageconfig> {

}
