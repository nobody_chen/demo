package com.chenyong.demo.service;

import com.chenyong.demo.entity.Business;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商家 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IBusinessService extends IService<Business> {

}
