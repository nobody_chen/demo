package com.chenyong.demo.service;

import com.chenyong.demo.entity.Admin;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 管理员 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAdminService extends IService<Admin> {

}
