package com.chenyong.demo.service;

import com.chenyong.demo.entity.Orderreturnsitem;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 退货项 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrderreturnsitemService extends IService<Orderreturnsitem> {

}
