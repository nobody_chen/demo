package com.chenyong.demo.service;

import com.chenyong.demo.entity.Users;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IUsersService extends IService<Users> {

}
