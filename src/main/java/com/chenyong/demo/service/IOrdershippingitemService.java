package com.chenyong.demo.service;

import com.chenyong.demo.entity.Ordershippingitem;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 发货项 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrdershippingitemService extends IService<Ordershippingitem> {

}
