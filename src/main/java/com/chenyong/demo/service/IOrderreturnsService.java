package com.chenyong.demo.service;

import com.chenyong.demo.entity.Orderreturns;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单退货 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrderreturnsService extends IService<Orderreturns> {

}
