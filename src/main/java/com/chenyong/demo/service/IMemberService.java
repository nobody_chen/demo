package com.chenyong.demo.service;

import com.chenyong.demo.entity.Member;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IMemberService extends IService<Member> {

}
