package com.chenyong.demo.service;

import com.chenyong.demo.entity.Receiver;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 收货地址 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IReceiverService extends IService<Receiver> {

}
