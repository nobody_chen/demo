package com.chenyong.demo.service;

import com.chenyong.demo.entity.Paymenttransaction;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 支付事务 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPaymenttransactionService extends IService<Paymenttransaction> {

}
