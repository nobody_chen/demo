package com.chenyong.demo.service;

import com.chenyong.demo.entity.Consultation;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 咨询 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IConsultationService extends IService<Consultation> {

}
