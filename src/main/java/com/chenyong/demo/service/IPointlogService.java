package com.chenyong.demo.service;

import com.chenyong.demo.entity.Pointlog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 积分记录 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPointlogService extends IService<Pointlog> {

}
