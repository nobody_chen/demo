package com.chenyong.demo.service;

import com.chenyong.demo.entity.Ordershipping;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单发货 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrdershippingService extends IService<Ordershipping> {

}
