package com.chenyong.demo.service;

import com.chenyong.demo.entity.Friendlink;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 友情链接 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IFriendlinkService extends IService<Friendlink> {

}
