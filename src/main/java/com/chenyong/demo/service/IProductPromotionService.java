package com.chenyong.demo.service;

import com.chenyong.demo.entity.ProductPromotion;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品促销中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductPromotionService extends IService<ProductPromotion> {

}
