package com.chenyong.demo.service;

import com.chenyong.demo.entity.Pluginconfig;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 插件配置 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPluginconfigService extends IService<Pluginconfig> {

}
