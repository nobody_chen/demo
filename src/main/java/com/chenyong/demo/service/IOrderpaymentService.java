package com.chenyong.demo.service;

import com.chenyong.demo.entity.Orderpayment;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单支付 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrderpaymentService extends IService<Orderpayment> {

}
