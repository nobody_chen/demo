package com.chenyong.demo.service;

import com.chenyong.demo.entity.Cash;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 提现 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ICashService extends IService<Cash> {

}
