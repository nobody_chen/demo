package com.chenyong.demo.service;

import com.chenyong.demo.entity.Businessdepositlog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商家预存款记录 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IBusinessdepositlogService extends IService<Businessdepositlog> {

}
