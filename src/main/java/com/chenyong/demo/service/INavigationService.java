package com.chenyong.demo.service;

import com.chenyong.demo.entity.Navigation;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 导航 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface INavigationService extends IService<Navigation> {

}
