package com.chenyong.demo.service;

import com.chenyong.demo.entity.Businessattribute;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商家注册项 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IBusinessattributeService extends IService<Businessattribute> {

}
