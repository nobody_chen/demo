package com.chenyong.demo.service;

import com.chenyong.demo.entity.PromotionSku;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 促销SKU中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPromotionSkuService extends IService<PromotionSku> {

}
