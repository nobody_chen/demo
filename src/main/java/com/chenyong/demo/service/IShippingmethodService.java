package com.chenyong.demo.service;

import com.chenyong.demo.entity.Shippingmethod;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 配送方式 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IShippingmethodService extends IService<Shippingmethod> {

}
