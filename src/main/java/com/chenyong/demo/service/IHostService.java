package com.chenyong.demo.service;

import com.chenyong.demo.entity.Host;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 域名配置 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IHostService extends IService<Host> {

}
