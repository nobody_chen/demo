package com.chenyong.demo.service;

import com.chenyong.demo.entity.Stocklog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 库存记录 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IStocklogService extends IService<Stocklog> {

}
