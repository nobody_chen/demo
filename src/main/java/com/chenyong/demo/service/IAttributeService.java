package com.chenyong.demo.service;

import com.chenyong.demo.entity.Attribute;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 属性 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAttributeService extends IService<Attribute> {

}
