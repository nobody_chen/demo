package com.chenyong.demo.service;

import com.chenyong.demo.entity.Paymentmethod;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 支付方式 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPaymentmethodService extends IService<Paymentmethod> {

}
