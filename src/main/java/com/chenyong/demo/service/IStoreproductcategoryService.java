package com.chenyong.demo.service;

import com.chenyong.demo.entity.Storeproductcategory;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 店铺商品分类 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IStoreproductcategoryService extends IService<Storeproductcategory> {

}
