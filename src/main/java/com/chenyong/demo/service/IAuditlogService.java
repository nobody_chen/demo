package com.chenyong.demo.service;

import com.chenyong.demo.entity.Auditlog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 审计日志 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAuditlogService extends IService<Auditlog> {

}
