package com.chenyong.demo.service;

import com.chenyong.demo.entity.Articletag;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 文章标签 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IArticletagService extends IService<Articletag> {

}
