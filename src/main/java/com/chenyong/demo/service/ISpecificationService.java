package com.chenyong.demo.service;

import com.chenyong.demo.entity.Specification;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 规格 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ISpecificationService extends IService<Specification> {

}
