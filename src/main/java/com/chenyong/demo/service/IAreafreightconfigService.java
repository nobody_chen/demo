package com.chenyong.demo.service;

import com.chenyong.demo.entity.Areafreightconfig;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 地区运费配置 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAreafreightconfigService extends IService<Areafreightconfig> {

}
