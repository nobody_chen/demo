package com.chenyong.demo.service;

import com.chenyong.demo.entity.Article;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 文章 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IArticleService extends IService<Article> {

}
