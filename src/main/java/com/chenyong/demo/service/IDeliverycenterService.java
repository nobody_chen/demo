package com.chenyong.demo.service;

import com.chenyong.demo.entity.Deliverycenter;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 发货点 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IDeliverycenterService extends IService<Deliverycenter> {

}
