package com.chenyong.demo.service;

import com.chenyong.demo.entity.Adposition;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 广告位 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAdpositionService extends IService<Adposition> {

}
