package com.chenyong.demo.service;

import com.chenyong.demo.entity.Promotionpluginsvc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 促销插件服务 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPromotionpluginsvcService extends IService<Promotionpluginsvc> {

}
