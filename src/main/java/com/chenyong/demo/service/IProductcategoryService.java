package com.chenyong.demo.service;

import com.chenyong.demo.entity.Productcategory;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品分类 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductcategoryService extends IService<Productcategory> {

}
