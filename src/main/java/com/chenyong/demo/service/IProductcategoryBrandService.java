package com.chenyong.demo.service;

import com.chenyong.demo.entity.ProductcategoryBrand;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品分类品牌中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductcategoryBrandService extends IService<ProductcategoryBrand> {

}
