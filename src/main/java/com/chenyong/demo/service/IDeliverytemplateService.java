package com.chenyong.demo.service;

import com.chenyong.demo.entity.Deliverytemplate;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 快递单模板 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IDeliverytemplateService extends IService<Deliverytemplate> {

}
