package com.chenyong.demo.service;

import com.chenyong.demo.entity.Message;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 消息 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IMessageService extends IService<Message> {

}
