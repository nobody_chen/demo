package com.chenyong.demo.service;

import com.chenyong.demo.entity.Storefavorite;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 店铺收藏 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IStorefavoriteService extends IService<Storefavorite> {

}
