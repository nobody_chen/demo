package com.chenyong.demo.service;

import com.chenyong.demo.entity.Orderitem;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单项 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrderitemService extends IService<Orderitem> {

}
