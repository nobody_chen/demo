package com.chenyong.demo.service;

import com.chenyong.demo.entity.Productbatch;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品批次 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductbatchService extends IService<Productbatch> {

}
