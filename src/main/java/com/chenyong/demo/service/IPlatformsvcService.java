package com.chenyong.demo.service;

import com.chenyong.demo.entity.Platformsvc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 平台服务 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IPlatformsvcService extends IService<Platformsvc> {

}
