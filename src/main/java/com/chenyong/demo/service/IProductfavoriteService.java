package com.chenyong.demo.service;

import com.chenyong.demo.entity.Productfavorite;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品收藏 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductfavoriteService extends IService<Productfavorite> {

}
