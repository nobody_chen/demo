package com.chenyong.demo.service;

import com.chenyong.demo.entity.ProductcategoryPromotion;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品分类促销中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductcategoryPromotionService extends IService<ProductcategoryPromotion> {

}
