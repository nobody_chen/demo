package com.chenyong.demo.service;

import com.chenyong.demo.entity.Cartitem;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 购物车项 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ICartitemService extends IService<Cartitem> {

}
