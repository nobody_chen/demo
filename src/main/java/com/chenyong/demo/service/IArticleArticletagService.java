package com.chenyong.demo.service;

import com.chenyong.demo.entity.ArticleArticletag;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 文章文章标签中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IArticleArticletagService extends IService<ArticleArticletag> {

}
