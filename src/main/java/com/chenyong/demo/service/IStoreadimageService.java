package com.chenyong.demo.service;

import com.chenyong.demo.entity.Storeadimage;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 店铺广告图片 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IStoreadimageService extends IService<Storeadimage> {

}
