package com.chenyong.demo.service;

import com.chenyong.demo.entity.Svc;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 服务 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ISvcService extends IService<Svc> {

}
