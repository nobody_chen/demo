package com.chenyong.demo.service;

import com.chenyong.demo.entity.ShippingmethodPaymentmethod;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 配送方式支付方式中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IShippingmethodPaymentmethodService extends IService<ShippingmethodPaymentmethod> {

}
