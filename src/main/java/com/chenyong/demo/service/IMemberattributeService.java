package com.chenyong.demo.service;

import com.chenyong.demo.entity.Memberattribute;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员注册项 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IMemberattributeService extends IService<Memberattribute> {

}
