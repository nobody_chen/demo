package com.chenyong.demo.service;

import com.chenyong.demo.entity.Producttag;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品标签 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProducttagService extends IService<Producttag> {

}
