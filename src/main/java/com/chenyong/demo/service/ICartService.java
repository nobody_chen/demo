package com.chenyong.demo.service;

import com.chenyong.demo.entity.Cart;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 购物车 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ICartService extends IService<Cart> {

}
