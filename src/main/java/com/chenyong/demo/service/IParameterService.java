package com.chenyong.demo.service;

import com.chenyong.demo.entity.Parameter;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 参数 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IParameterService extends IService<Parameter> {

}
