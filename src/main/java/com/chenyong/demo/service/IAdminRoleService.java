package com.chenyong.demo.service;

import com.chenyong.demo.entity.AdminRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 管理员角色中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAdminRoleService extends IService<AdminRole> {

}
