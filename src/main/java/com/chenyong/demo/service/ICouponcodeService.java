package com.chenyong.demo.service;

import com.chenyong.demo.entity.Couponcode;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 优惠码 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ICouponcodeService extends IService<Couponcode> {

}
