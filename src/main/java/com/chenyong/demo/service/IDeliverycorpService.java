package com.chenyong.demo.service;

import com.chenyong.demo.entity.Deliverycorp;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 物流公司 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IDeliverycorpService extends IService<Deliverycorp> {

}
