package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orderreturnsitem;
import com.chenyong.demo.mapper.OrderreturnsitemMapper;
import com.chenyong.demo.service.IOrderreturnsitemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 退货项 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrderreturnsitemServiceImpl extends ServiceImpl<OrderreturnsitemMapper, Orderreturnsitem> implements IOrderreturnsitemService {

}
