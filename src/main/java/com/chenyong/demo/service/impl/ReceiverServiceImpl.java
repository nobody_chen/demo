package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Receiver;
import com.chenyong.demo.mapper.ReceiverMapper;
import com.chenyong.demo.service.IReceiverService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货地址 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ReceiverServiceImpl extends ServiceImpl<ReceiverMapper, Receiver> implements IReceiverService {

}
