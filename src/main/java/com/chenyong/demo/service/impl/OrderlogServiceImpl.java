package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orderlog;
import com.chenyong.demo.mapper.OrderlogMapper;
import com.chenyong.demo.service.IOrderlogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单记录 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrderlogServiceImpl extends ServiceImpl<OrderlogMapper, Orderlog> implements IOrderlogService {

}
