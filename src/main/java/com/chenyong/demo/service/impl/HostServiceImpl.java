package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Host;
import com.chenyong.demo.mapper.HostMapper;
import com.chenyong.demo.service.IHostService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 域名配置 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class HostServiceImpl extends ServiceImpl<HostMapper, Host> implements IHostService {

}
