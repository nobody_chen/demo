package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orderpayment;
import com.chenyong.demo.mapper.OrderpaymentMapper;
import com.chenyong.demo.service.IOrderpaymentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单支付 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrderpaymentServiceImpl extends ServiceImpl<OrderpaymentMapper, Orderpayment> implements IOrderpaymentService {

}
