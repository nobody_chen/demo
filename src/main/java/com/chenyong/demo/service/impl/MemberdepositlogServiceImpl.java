package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Memberdepositlog;
import com.chenyong.demo.mapper.MemberdepositlogMapper;
import com.chenyong.demo.service.IMemberdepositlogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员预存款记录 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class MemberdepositlogServiceImpl extends ServiceImpl<MemberdepositlogMapper, Memberdepositlog> implements IMemberdepositlogService {

}
