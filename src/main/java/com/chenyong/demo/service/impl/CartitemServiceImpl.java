package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Cartitem;
import com.chenyong.demo.mapper.CartitemMapper;
import com.chenyong.demo.service.ICartitemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车项 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class CartitemServiceImpl extends ServiceImpl<CartitemMapper, Cartitem> implements ICartitemService {

}
