package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Pointlog;
import com.chenyong.demo.mapper.PointlogMapper;
import com.chenyong.demo.service.IPointlogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 积分记录 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PointlogServiceImpl extends ServiceImpl<PointlogMapper, Pointlog> implements IPointlogService {

}
