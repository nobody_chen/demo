package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Brand;
import com.chenyong.demo.mapper.BrandMapper;
import com.chenyong.demo.service.IBrandService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements IBrandService {

}
