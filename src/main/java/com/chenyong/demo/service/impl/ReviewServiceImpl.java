package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Review;
import com.chenyong.demo.mapper.ReviewMapper;
import com.chenyong.demo.service.IReviewService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ReviewServiceImpl extends ServiceImpl<ReviewMapper, Review> implements IReviewService {

}
