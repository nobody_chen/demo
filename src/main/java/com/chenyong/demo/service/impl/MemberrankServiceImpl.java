package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Memberrank;
import com.chenyong.demo.mapper.MemberrankMapper;
import com.chenyong.demo.service.IMemberrankService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员等级 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class MemberrankServiceImpl extends ServiceImpl<MemberrankMapper, Memberrank> implements IMemberrankService {

}
