package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Deliverytemplate;
import com.chenyong.demo.mapper.DeliverytemplateMapper;
import com.chenyong.demo.service.IDeliverytemplateService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 快递单模板 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class DeliverytemplateServiceImpl extends ServiceImpl<DeliverytemplateMapper, Deliverytemplate> implements IDeliverytemplateService {

}
