package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ProductPromotion;
import com.chenyong.demo.mapper.ProductPromotionMapper;
import com.chenyong.demo.service.IProductPromotionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品促销中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductPromotionServiceImpl extends ServiceImpl<ProductPromotionMapper, ProductPromotion> implements IProductPromotionService {

}
