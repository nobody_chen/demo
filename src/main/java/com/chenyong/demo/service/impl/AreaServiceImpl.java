package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Area;
import com.chenyong.demo.mapper.AreaMapper;
import com.chenyong.demo.service.IAreaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地区 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements IAreaService {

}
