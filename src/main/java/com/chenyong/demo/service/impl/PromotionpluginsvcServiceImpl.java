package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Promotionpluginsvc;
import com.chenyong.demo.mapper.PromotionpluginsvcMapper;
import com.chenyong.demo.service.IPromotionpluginsvcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销插件服务 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PromotionpluginsvcServiceImpl extends ServiceImpl<PromotionpluginsvcMapper, Promotionpluginsvc> implements IPromotionpluginsvcService {

}
