package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Member;
import com.chenyong.demo.mapper.MemberMapper;
import com.chenyong.demo.service.IMemberService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements IMemberService {

}
