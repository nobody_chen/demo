package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Statistic;
import com.chenyong.demo.mapper.StatisticMapper;
import com.chenyong.demo.service.IStatisticService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 统计 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StatisticServiceImpl extends ServiceImpl<StatisticMapper, Statistic> implements IStatisticService {

}
