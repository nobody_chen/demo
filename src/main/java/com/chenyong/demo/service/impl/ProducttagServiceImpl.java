package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Producttag;
import com.chenyong.demo.mapper.ProducttagMapper;
import com.chenyong.demo.service.IProducttagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品标签 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProducttagServiceImpl extends ServiceImpl<ProducttagMapper, Producttag> implements IProducttagService {

}
