package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Productfavorite;
import com.chenyong.demo.mapper.ProductfavoriteMapper;
import com.chenyong.demo.service.IProductfavoriteService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品收藏 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductfavoriteServiceImpl extends ServiceImpl<ProductfavoriteMapper, Productfavorite> implements IProductfavoriteService {

}
