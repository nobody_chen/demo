package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Cash;
import com.chenyong.demo.mapper.CashMapper;
import com.chenyong.demo.service.ICashService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 提现 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class CashServiceImpl extends ServiceImpl<CashMapper, Cash> implements ICashService {

}
