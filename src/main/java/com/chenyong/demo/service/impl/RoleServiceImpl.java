package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Role;
import com.chenyong.demo.mapper.RoleMapper;
import com.chenyong.demo.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

}
