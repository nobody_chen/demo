package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Storefavorite;
import com.chenyong.demo.mapper.StorefavoriteMapper;
import com.chenyong.demo.service.IStorefavoriteService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺收藏 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StorefavoriteServiceImpl extends ServiceImpl<StorefavoriteMapper, Storefavorite> implements IStorefavoriteService {

}
