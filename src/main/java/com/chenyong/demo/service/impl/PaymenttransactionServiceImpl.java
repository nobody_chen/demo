package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Paymenttransaction;
import com.chenyong.demo.mapper.PaymenttransactionMapper;
import com.chenyong.demo.service.IPaymenttransactionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付事务 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PaymenttransactionServiceImpl extends ServiceImpl<PaymenttransactionMapper, Paymenttransaction> implements IPaymenttransactionService {

}
