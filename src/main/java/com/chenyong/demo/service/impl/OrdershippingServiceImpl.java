package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Ordershipping;
import com.chenyong.demo.mapper.OrdershippingMapper;
import com.chenyong.demo.service.IOrdershippingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单发货 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrdershippingServiceImpl extends ServiceImpl<OrdershippingMapper, Ordershipping> implements IOrdershippingService {

}
