package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Specification;
import com.chenyong.demo.mapper.SpecificationMapper;
import com.chenyong.demo.service.ISpecificationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规格 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class SpecificationServiceImpl extends ServiceImpl<SpecificationMapper, Specification> implements ISpecificationService {

}
