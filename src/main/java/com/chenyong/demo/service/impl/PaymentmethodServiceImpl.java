package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Paymentmethod;
import com.chenyong.demo.mapper.PaymentmethodMapper;
import com.chenyong.demo.service.IPaymentmethodService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 支付方式 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PaymentmethodServiceImpl extends ServiceImpl<PaymentmethodMapper, Paymentmethod> implements IPaymentmethodService {

}
