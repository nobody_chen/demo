package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Deliverycenter;
import com.chenyong.demo.mapper.DeliverycenterMapper;
import com.chenyong.demo.service.IDeliverycenterService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发货点 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class DeliverycenterServiceImpl extends ServiceImpl<DeliverycenterMapper, Deliverycenter> implements IDeliverycenterService {

}
