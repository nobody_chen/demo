package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Productnotify;
import com.chenyong.demo.mapper.ProductnotifyMapper;
import com.chenyong.demo.service.IProductnotifyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 到货通知 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductnotifyServiceImpl extends ServiceImpl<ProductnotifyMapper, Productnotify> implements IProductnotifyService {

}
