package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Product;
import com.chenyong.demo.mapper.ProductMapper;
import com.chenyong.demo.service.IProductService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

}
