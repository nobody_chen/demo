package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Articlecategory;
import com.chenyong.demo.mapper.ArticlecategoryMapper;
import com.chenyong.demo.service.IArticlecategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章分类 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ArticlecategoryServiceImpl extends ServiceImpl<ArticlecategoryMapper, Articlecategory> implements IArticlecategoryService {

}
