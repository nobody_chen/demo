package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orderrefunds;
import com.chenyong.demo.mapper.OrderrefundsMapper;
import com.chenyong.demo.service.IOrderrefundsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单退款 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrderrefundsServiceImpl extends ServiceImpl<OrderrefundsMapper, Orderrefunds> implements IOrderrefundsService {

}
