package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Instantmessage;
import com.chenyong.demo.mapper.InstantmessageMapper;
import com.chenyong.demo.service.IInstantmessageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 即时通讯 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class InstantmessageServiceImpl extends ServiceImpl<InstantmessageMapper, Instantmessage> implements IInstantmessageService {

}
