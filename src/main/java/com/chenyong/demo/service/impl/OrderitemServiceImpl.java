package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orderitem;
import com.chenyong.demo.mapper.OrderitemMapper;
import com.chenyong.demo.service.IOrderitemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单项 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrderitemServiceImpl extends ServiceImpl<OrderitemMapper, Orderitem> implements IOrderitemService {

}
