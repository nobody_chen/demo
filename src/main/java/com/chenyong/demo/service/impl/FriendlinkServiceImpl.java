package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Friendlink;
import com.chenyong.demo.mapper.FriendlinkMapper;
import com.chenyong.demo.service.IFriendlinkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 友情链接 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class FriendlinkServiceImpl extends ServiceImpl<FriendlinkMapper, Friendlink> implements IFriendlinkService {

}
