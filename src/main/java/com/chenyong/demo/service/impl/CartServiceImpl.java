package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Cart;
import com.chenyong.demo.mapper.CartMapper;
import com.chenyong.demo.service.ICartService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 购物车 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

}
