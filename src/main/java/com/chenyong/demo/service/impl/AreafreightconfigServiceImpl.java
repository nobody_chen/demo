package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Areafreightconfig;
import com.chenyong.demo.mapper.AreafreightconfigMapper;
import com.chenyong.demo.service.IAreafreightconfigService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地区运费配置 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AreafreightconfigServiceImpl extends ServiceImpl<AreafreightconfigMapper, Areafreightconfig> implements IAreafreightconfigService {

}
