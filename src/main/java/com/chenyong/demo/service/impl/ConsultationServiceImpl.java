package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Consultation;
import com.chenyong.demo.mapper.ConsultationMapper;
import com.chenyong.demo.service.IConsultationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 咨询 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ConsultationServiceImpl extends ServiceImpl<ConsultationMapper, Consultation> implements IConsultationService {

}
