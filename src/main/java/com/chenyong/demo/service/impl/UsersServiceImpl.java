package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Users;
import com.chenyong.demo.mapper.UsersMapper;
import com.chenyong.demo.service.IUsersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Users> implements IUsersService {

}
