package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ProductStoreproducttag;
import com.chenyong.demo.mapper.ProductStoreproducttagMapper;
import com.chenyong.demo.service.IProductStoreproducttagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品店铺商品标签中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductStoreproducttagServiceImpl extends ServiceImpl<ProductStoreproducttagMapper, ProductStoreproducttag> implements IProductStoreproducttagService {

}
