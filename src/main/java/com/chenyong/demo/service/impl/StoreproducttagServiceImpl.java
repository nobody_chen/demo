package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Storeproducttag;
import com.chenyong.demo.mapper.StoreproducttagMapper;
import com.chenyong.demo.service.IStoreproducttagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StoreproducttagServiceImpl extends ServiceImpl<StoreproducttagMapper, Storeproducttag> implements IStoreproducttagService {

}
