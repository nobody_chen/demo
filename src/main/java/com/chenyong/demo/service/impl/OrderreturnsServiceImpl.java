package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orderreturns;
import com.chenyong.demo.mapper.OrderreturnsMapper;
import com.chenyong.demo.service.IOrderreturnsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单退货 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrderreturnsServiceImpl extends ServiceImpl<OrderreturnsMapper, Orderreturns> implements IOrderreturnsService {

}
