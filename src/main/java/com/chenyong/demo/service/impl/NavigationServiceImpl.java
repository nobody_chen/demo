package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Navigation;
import com.chenyong.demo.mapper.NavigationMapper;
import com.chenyong.demo.service.INavigationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 导航 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class NavigationServiceImpl extends ServiceImpl<NavigationMapper, Navigation> implements INavigationService {

}
