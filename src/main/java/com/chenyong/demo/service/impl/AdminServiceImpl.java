package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Admin;
import com.chenyong.demo.mapper.AdminMapper;
import com.chenyong.demo.service.IAdminService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

}
