package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Businessattribute;
import com.chenyong.demo.mapper.BusinessattributeMapper;
import com.chenyong.demo.service.IBusinessattributeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商家注册项 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class BusinessattributeServiceImpl extends ServiceImpl<BusinessattributeMapper, Businessattribute> implements IBusinessattributeService {

}
