package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Memberattribute;
import com.chenyong.demo.mapper.MemberattributeMapper;
import com.chenyong.demo.service.IMemberattributeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员注册项 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class MemberattributeServiceImpl extends ServiceImpl<MemberattributeMapper, Memberattribute> implements IMemberattributeService {

}
