package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Svc;
import com.chenyong.demo.mapper.SvcMapper;
import com.chenyong.demo.service.ISvcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class SvcServiceImpl extends ServiceImpl<SvcMapper, Svc> implements ISvcService {

}
