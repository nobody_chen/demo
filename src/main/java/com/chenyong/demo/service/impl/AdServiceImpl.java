package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Ad;
import com.chenyong.demo.mapper.AdMapper;
import com.chenyong.demo.service.IAdService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 广告 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AdServiceImpl extends ServiceImpl<AdMapper, Ad> implements IAdService {

}
