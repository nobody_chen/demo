package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Couponcode;
import com.chenyong.demo.mapper.CouponcodeMapper;
import com.chenyong.demo.service.ICouponcodeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠码 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class CouponcodeServiceImpl extends ServiceImpl<CouponcodeMapper, Couponcode> implements ICouponcodeService {

}
