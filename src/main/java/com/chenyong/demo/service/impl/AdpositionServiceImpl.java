package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Adposition;
import com.chenyong.demo.mapper.AdpositionMapper;
import com.chenyong.demo.service.IAdpositionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 广告位 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AdpositionServiceImpl extends ServiceImpl<AdpositionMapper, Adposition> implements IAdpositionService {

}
