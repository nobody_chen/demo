package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Articletag;
import com.chenyong.demo.mapper.ArticletagMapper;
import com.chenyong.demo.service.IArticletagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章标签 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ArticletagServiceImpl extends ServiceImpl<ArticletagMapper, Articletag> implements IArticletagService {

}
