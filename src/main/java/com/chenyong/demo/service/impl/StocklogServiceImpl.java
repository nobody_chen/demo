package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Stocklog;
import com.chenyong.demo.mapper.StocklogMapper;
import com.chenyong.demo.service.IStocklogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存记录 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StocklogServiceImpl extends ServiceImpl<StocklogMapper, Stocklog> implements IStocklogService {

}
