package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Parameter;
import com.chenyong.demo.mapper.ParameterMapper;
import com.chenyong.demo.service.IParameterService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ParameterServiceImpl extends ServiceImpl<ParameterMapper, Parameter> implements IParameterService {

}
