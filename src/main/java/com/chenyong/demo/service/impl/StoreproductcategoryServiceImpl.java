package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Storeproductcategory;
import com.chenyong.demo.mapper.StoreproductcategoryMapper;
import com.chenyong.demo.service.IStoreproductcategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺商品分类 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StoreproductcategoryServiceImpl extends ServiceImpl<StoreproductcategoryMapper, Storeproductcategory> implements IStoreproductcategoryService {

}
