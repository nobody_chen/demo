package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Shippingmethod;
import com.chenyong.demo.mapper.ShippingmethodMapper;
import com.chenyong.demo.service.IShippingmethodService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配送方式 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ShippingmethodServiceImpl extends ServiceImpl<ShippingmethodMapper, Shippingmethod> implements IShippingmethodService {

}
