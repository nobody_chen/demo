package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Storeadimage;
import com.chenyong.demo.mapper.StoreadimageMapper;
import com.chenyong.demo.service.IStoreadimageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺广告图片 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StoreadimageServiceImpl extends ServiceImpl<StoreadimageMapper, Storeadimage> implements IStoreadimageService {

}
