package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.PromotionMemberrank;
import com.chenyong.demo.mapper.PromotionMemberrankMapper;
import com.chenyong.demo.service.IPromotionMemberrankService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销会员等级中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PromotionMemberrankServiceImpl extends ServiceImpl<PromotionMemberrankMapper, PromotionMemberrank> implements IPromotionMemberrankService {

}
