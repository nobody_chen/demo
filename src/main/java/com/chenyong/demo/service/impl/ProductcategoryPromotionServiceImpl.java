package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ProductcategoryPromotion;
import com.chenyong.demo.mapper.ProductcategoryPromotionMapper;
import com.chenyong.demo.service.IProductcategoryPromotionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类促销中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductcategoryPromotionServiceImpl extends ServiceImpl<ProductcategoryPromotionMapper, ProductcategoryPromotion> implements IProductcategoryPromotionService {

}
