package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ProductProducttag;
import com.chenyong.demo.mapper.ProductProducttagMapper;
import com.chenyong.demo.service.IProductProducttagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品商品标签中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductProducttagServiceImpl extends ServiceImpl<ProductProducttagMapper, ProductProducttag> implements IProductProducttagService {

}
