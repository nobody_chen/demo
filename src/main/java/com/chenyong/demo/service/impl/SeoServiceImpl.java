package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Seo;
import com.chenyong.demo.mapper.SeoMapper;
import com.chenyong.demo.service.ISeoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * SEO设置 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class SeoServiceImpl extends ServiceImpl<SeoMapper, Seo> implements ISeoService {

}
