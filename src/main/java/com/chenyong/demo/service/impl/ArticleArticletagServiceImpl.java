package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ArticleArticletag;
import com.chenyong.demo.mapper.ArticleArticletagMapper;
import com.chenyong.demo.service.IArticleArticletagService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章文章标签中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ArticleArticletagServiceImpl extends ServiceImpl<ArticleArticletagMapper, ArticleArticletag> implements IArticleArticletagService {

}
