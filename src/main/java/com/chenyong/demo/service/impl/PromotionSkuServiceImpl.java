package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.PromotionSku;
import com.chenyong.demo.mapper.PromotionSkuMapper;
import com.chenyong.demo.service.IPromotionSkuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销SKU中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PromotionSkuServiceImpl extends ServiceImpl<PromotionSkuMapper, PromotionSku> implements IPromotionSkuService {

}
