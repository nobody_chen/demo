package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Productbatch;
import com.chenyong.demo.mapper.ProductbatchMapper;
import com.chenyong.demo.service.IProductbatchService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品批次 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductbatchServiceImpl extends ServiceImpl<ProductbatchMapper, Productbatch> implements IProductbatchService {

}
