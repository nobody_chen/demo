package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Store;
import com.chenyong.demo.mapper.StoreMapper;
import com.chenyong.demo.service.IStoreService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements IStoreService {

}
