package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Ordershippingitem;
import com.chenyong.demo.mapper.OrdershippingitemMapper;
import com.chenyong.demo.service.IOrdershippingitemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发货项 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrdershippingitemServiceImpl extends ServiceImpl<OrdershippingitemMapper, Ordershippingitem> implements IOrdershippingitemService {

}
