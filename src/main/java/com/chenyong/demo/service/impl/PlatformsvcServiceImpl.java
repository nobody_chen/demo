package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Platformsvc;
import com.chenyong.demo.mapper.PlatformsvcMapper;
import com.chenyong.demo.service.IPlatformsvcService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台服务 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PlatformsvcServiceImpl extends ServiceImpl<PlatformsvcMapper, Platformsvc> implements IPlatformsvcService {

}
