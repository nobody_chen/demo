package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ShippingmethodPaymentmethod;
import com.chenyong.demo.mapper.ShippingmethodPaymentmethodMapper;
import com.chenyong.demo.service.IShippingmethodPaymentmethodService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 配送方式支付方式中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ShippingmethodPaymentmethodServiceImpl extends ServiceImpl<ShippingmethodPaymentmethodMapper, ShippingmethodPaymentmethod> implements IShippingmethodPaymentmethodService {

}
