package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.OrdersCoupon;
import com.chenyong.demo.mapper.OrdersCouponMapper;
import com.chenyong.demo.service.IOrdersCouponService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单优惠券中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrdersCouponServiceImpl extends ServiceImpl<OrdersCouponMapper, OrdersCoupon> implements IOrdersCouponService {

}
