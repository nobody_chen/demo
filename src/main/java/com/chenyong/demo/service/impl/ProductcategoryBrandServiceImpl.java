package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.ProductcategoryBrand;
import com.chenyong.demo.mapper.ProductcategoryBrandMapper;
import com.chenyong.demo.service.IProductcategoryBrandService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类品牌中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductcategoryBrandServiceImpl extends ServiceImpl<ProductcategoryBrandMapper, ProductcategoryBrand> implements IProductcategoryBrandService {

}
