package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Socialuser;
import com.chenyong.demo.mapper.SocialuserMapper;
import com.chenyong.demo.service.ISocialuserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 社会化用户 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class SocialuserServiceImpl extends ServiceImpl<SocialuserMapper, Socialuser> implements ISocialuserService {

}
