package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.PromotionCoupon;
import com.chenyong.demo.mapper.PromotionCouponMapper;
import com.chenyong.demo.service.IPromotionCouponService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销优惠券中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PromotionCouponServiceImpl extends ServiceImpl<PromotionCouponMapper, PromotionCoupon> implements IPromotionCouponService {

}
