package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Sku;
import com.chenyong.demo.mapper.SkuMapper;
import com.chenyong.demo.service.ISkuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * SKU 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku> implements ISkuService {

}
