package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Article;
import com.chenyong.demo.mapper.ArticleMapper;
import com.chenyong.demo.service.IArticleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文章 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

}
