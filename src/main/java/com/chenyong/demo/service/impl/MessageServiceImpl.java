package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Message;
import com.chenyong.demo.mapper.MessageMapper;
import com.chenyong.demo.service.IMessageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {

}
