package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Deliverycorp;
import com.chenyong.demo.mapper.DeliverycorpMapper;
import com.chenyong.demo.service.IDeliverycorpService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 物流公司 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class DeliverycorpServiceImpl extends ServiceImpl<DeliverycorpMapper, Deliverycorp> implements IDeliverycorpService {

}
