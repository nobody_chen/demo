package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Sn;
import com.chenyong.demo.mapper.SnMapper;
import com.chenyong.demo.service.ISnService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class SnServiceImpl extends ServiceImpl<SnMapper, Sn> implements ISnService {

}
