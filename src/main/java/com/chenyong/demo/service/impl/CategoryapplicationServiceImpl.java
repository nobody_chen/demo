package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Categoryapplication;
import com.chenyong.demo.mapper.CategoryapplicationMapper;
import com.chenyong.demo.service.ICategoryapplicationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 经营分类申请 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class CategoryapplicationServiceImpl extends ServiceImpl<CategoryapplicationMapper, Categoryapplication> implements ICategoryapplicationService {

}
