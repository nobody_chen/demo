package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Productcategory;
import com.chenyong.demo.mapper.ProductcategoryMapper;
import com.chenyong.demo.service.IProductcategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品分类 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class ProductcategoryServiceImpl extends ServiceImpl<ProductcategoryMapper, Productcategory> implements IProductcategoryService {

}
