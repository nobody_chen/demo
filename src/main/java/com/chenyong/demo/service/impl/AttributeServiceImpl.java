package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Attribute;
import com.chenyong.demo.mapper.AttributeMapper;
import com.chenyong.demo.service.IAttributeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 属性 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AttributeServiceImpl extends ServiceImpl<AttributeMapper, Attribute> implements IAttributeService {

}
