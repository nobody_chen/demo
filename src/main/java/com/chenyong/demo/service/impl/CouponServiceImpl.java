package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Coupon;
import com.chenyong.demo.mapper.CouponMapper;
import com.chenyong.demo.service.ICouponService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class CouponServiceImpl extends ServiceImpl<CouponMapper, Coupon> implements ICouponService {

}
