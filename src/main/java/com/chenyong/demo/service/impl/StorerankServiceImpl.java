package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Storerank;
import com.chenyong.demo.mapper.StorerankMapper;
import com.chenyong.demo.service.IStorerankService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺等级 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StorerankServiceImpl extends ServiceImpl<StorerankMapper, Storerank> implements IStorerankService {

}
