package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Auditlog;
import com.chenyong.demo.mapper.AuditlogMapper;
import com.chenyong.demo.service.IAuditlogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 审计日志 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AuditlogServiceImpl extends ServiceImpl<AuditlogMapper, Auditlog> implements IAuditlogService {

}
