package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Promotion;
import com.chenyong.demo.mapper.PromotionMapper;
import com.chenyong.demo.service.IPromotionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PromotionServiceImpl extends ServiceImpl<PromotionMapper, Promotion> implements IPromotionService {

}
