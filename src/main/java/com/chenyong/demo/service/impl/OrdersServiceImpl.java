package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Orders;
import com.chenyong.demo.mapper.OrdersMapper;
import com.chenyong.demo.service.IOrdersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

}
