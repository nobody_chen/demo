package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Pluginconfig;
import com.chenyong.demo.mapper.PluginconfigMapper;
import com.chenyong.demo.service.IPluginconfigService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 插件配置 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class PluginconfigServiceImpl extends ServiceImpl<PluginconfigMapper, Pluginconfig> implements IPluginconfigService {

}
