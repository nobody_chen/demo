package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Idgenerator;
import com.chenyong.demo.mapper.IdgeneratorMapper;
import com.chenyong.demo.service.IIdgeneratorService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * ID管理表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class IdgeneratorServiceImpl extends ServiceImpl<IdgeneratorMapper, Idgenerator> implements IIdgeneratorService {

}
