package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Storecategory;
import com.chenyong.demo.mapper.StorecategoryMapper;
import com.chenyong.demo.service.IStorecategoryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 店铺分类 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class StorecategoryServiceImpl extends ServiceImpl<StorecategoryMapper, Storecategory> implements IStorecategoryService {

}
