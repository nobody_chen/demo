package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.AdminRole;
import com.chenyong.demo.mapper.AdminRoleMapper;
import com.chenyong.demo.service.IAdminRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员角色中间表 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class AdminRoleServiceImpl extends ServiceImpl<AdminRoleMapper, AdminRole> implements IAdminRoleService {

}
