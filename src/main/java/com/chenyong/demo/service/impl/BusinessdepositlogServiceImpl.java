package com.chenyong.demo.service.impl;

import com.chenyong.demo.entity.Businessdepositlog;
import com.chenyong.demo.mapper.BusinessdepositlogMapper;
import com.chenyong.demo.service.IBusinessdepositlogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商家预存款记录 服务实现类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Service
public class BusinessdepositlogServiceImpl extends ServiceImpl<BusinessdepositlogMapper, Businessdepositlog> implements IBusinessdepositlogService {

}
