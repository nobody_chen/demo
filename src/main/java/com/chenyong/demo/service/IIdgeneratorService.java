package com.chenyong.demo.service;

import com.chenyong.demo.entity.Idgenerator;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * ID管理表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IIdgeneratorService extends IService<Idgenerator> {

}
