package com.chenyong.demo.service;

import com.chenyong.demo.entity.Orderrefunds;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 订单退款 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IOrderrefundsService extends IService<Orderrefunds> {

}
