package com.chenyong.demo.service;

import com.chenyong.demo.entity.ProductStoreproducttag;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品店铺商品标签中间表 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IProductStoreproducttagService extends IService<ProductStoreproducttag> {

}
