package com.chenyong.demo.service;

import com.chenyong.demo.entity.Instantmessage;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 即时通讯 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IInstantmessageService extends IService<Instantmessage> {

}
