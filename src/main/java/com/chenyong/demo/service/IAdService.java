package com.chenyong.demo.service;

import com.chenyong.demo.entity.Ad;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 广告 服务类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IAdService extends IService<Ad> {

}
