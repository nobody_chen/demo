package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 店铺等级
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Storerank extends Model<Storerank> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 是否允许注册
     */
    private Boolean isAllowRegister;
    /**
     * 备注
     */
    private String memo;
    /**
     * 名称
     */
    private String name;
    /**
     * 可发布商品数
     */
    private Long quantity;
    /**
     * 服务费
     */
    private BigDecimal serviceFee;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
