package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付事务
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Paymenttransaction extends Model<Paymenttransaction> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 金额
     */
    private BigDecimal amount;
    /**
     * 过期时间
     */
    private Date expire;
    /**
     * 支付手续费
     */
    private BigDecimal fee;
    /**
     * 是否成功
     */
    private Boolean isSuccess;
    /**
     * 支付插件ID
     */
    private String paymentPluginId;
    /**
     * 支付插件名称
     */
    private String paymentPluginName;
    /**
     * 编号
     */
    private String sn;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 订单
     */
    private Long orders;
    /**
     * 父事务
     */
    @TableField("parent_id")
    private Long parentId;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;
    /**
     * 服务
     */
    @TableField("svc_id")
    private Long svcId;
    /**
     * 用户
     */
    @TableField("user_id")
    private Long userId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
