package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员等级
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Memberrank extends Model<Memberrank> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 消费金额
     */
    private BigDecimal amount;
    /**
     * 是否默认
     */
    private Boolean isDefault;
    /**
     * 是否特殊
     */
    private Boolean isSpecial;
    /**
     * 名称
     */
    private String name;
    /**
     * 优惠比例
     */
    private Double scale;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
