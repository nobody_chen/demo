package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 促销
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Promotion extends Model<Promotion> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 起始日期
     */
    private Date beginDate;
    /**
     * 条件金额
     */
    private BigDecimal conditionsAmount;
    /**
     * 条件数量
     */
    private Integer conditionsNumber;
    /**
     * 减免金额
     */
    private BigDecimal creditAmount;
    /**
     * 满减数量
     */
    private Integer creditNumber;
    /**
     * 折扣
     */
    private Double discount;
    /**
     * 结束日期
     */
    private Date endDate;
    /**
     * 图片
     */
    private String image;
    /**
     * 介绍
     */
    private String introduction;
    /**
     * 是否允许使用优惠券
     */
    private Boolean isCouponAllowed;
    /**
     * 是否启用
     */
    private Boolean isEnabled;
    /**
     * 是否免运费
     */
    private Boolean isFreeShipping;
    /**
     * 最大SKU价格
     */
    private BigDecimal maximumPrice;
    /**
     * 最大SKU数量
     */
    private Integer maximumQuantity;
    /**
     * 最小SKU价格
     */
    private BigDecimal minimumPrice;
    /**
     * 最小SKU数量
     */
    private Integer minimumQuantity;
    /**
     * 名称
     */
    private String name;
    /**
     * 价格运算表达式
     */
    private String priceExpression;
    /**
     * 标题
     */
    private String title;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
