package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品店铺商品标签中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("product_storeproducttag")
public class ProductStoreproducttag extends Model<ProductStoreproducttag> {

    private static final long serialVersionUID = 1L;

    /**
     * 商品
     */
    @TableId("products_id")
    private Long productsId;
    /**
     * 店铺商品标签
     */
    @TableField("storeProductTags_id")
    private Long storeproducttagsId;


    @Override
    protected Serializable pkVal() {
        return this.productsId;
    }

}
