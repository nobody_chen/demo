package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单发货
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Ordershipping extends Model<Ordershipping> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 地址
     */
    private String address;
    /**
     * 地区
     */
    private String area;
    /**
     * 收货人
     */
    private String consignee;
    /**
     * 物流公司
     */
    private String deliveryCorp;
    /**
     * 物流公司代码
     */
    private String deliveryCorpCode;
    /**
     * 物流公司网址
     */
    private String deliveryCorpUrl;
    /**
     * 物流费用
     */
    private BigDecimal freight;
    /**
     * 备注
     */
    private String memo;
    /**
     * 电话
     */
    private String phone;
    /**
     * 配送方式
     */
    private String shippingMethod;
    /**
     * 编号
     */
    private String sn;
    /**
     * 运单号
     */
    private String trackingNo;
    /**
     * 邮编
     */
    private String zipCode;
    /**
     * 订单
     */
    private Long orders;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
