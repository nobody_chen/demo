package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员注册项
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Memberattribute extends Model<Memberattribute> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 是否启用
     */
    private Boolean isEnabled;
    /**
     * 是否必填
     */
    private Boolean isRequired;
    /**
     * 名称
     */
    private String name;
    /**
     * 可选项
     */
    private String options;
    /**
     * 配比
     */
    private String pattern;
    /**
     * 属性序号
     */
    private Integer propertyIndex;
    /**
     * 类型
     */
    private Integer type;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
