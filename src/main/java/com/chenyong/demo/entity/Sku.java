package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * SKU
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Sku extends Model<Sku> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 已分配库存
     */
    private Integer allocatedStock;
    /**
     * 成本价
     */
    private BigDecimal cost;
    /**
     * 兑换积分
     */
    private Long exchangePoint;
    /**
     * 是否默认
     */
    private Boolean isDefault;
    /**
     * 市场价
     */
    private BigDecimal marketPrice;
    /**
     * 销售价
     */
    private BigDecimal price;
    /**
     * 赠送积分
     */
    private Long rewardPoint;
    /**
     * 编号
     */
    private String sn;
    /**
     * 规格值
     */
    private String specificationValues;
    /**
     * 库存
     */
    private Integer stock;
    /**
     * 商品
     */
    @TableField("product_id")
    private Long productId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
