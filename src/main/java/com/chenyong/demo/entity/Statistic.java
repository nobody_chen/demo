package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 统计
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Statistic extends Model<Statistic> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 日
     */
    private Integer day;
    /**
     * 月
     */
    private Integer month;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 值
     */
    private BigDecimal value;
    /**
     * 年
     */
    private Integer year;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
