package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 优惠码
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Couponcode extends Model<Couponcode> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 号码
     */
    private String code;
    /**
     * 是否已使用
     */
    private Boolean isUsed;
    /**
     * 使用日期
     */
    private Date usedDate;
    /**
     * 优惠券
     */
    @TableField("coupon_id")
    private Long couponId;
    /**
     * 会员
     */
    @TableField("member_id")
    private Long memberId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
