package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 提现
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Cash extends Model<Cash> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 收款账号
     */
    private String account;
    /**
     * 金额
     */
    private BigDecimal amount;
    /**
     * 收款银行
     */
    private String bank;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 商家
     */
    @TableField("business_id")
    private Long businessId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
