package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 收货地址
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Receiver extends Model<Receiver> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 地址
     */
    private String address;
    /**
     * 地区名称
     */
    private String areaName;
    /**
     * 收货人
     */
    private String consignee;
    /**
     * 是否默认
     */
    private Boolean isDefault;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮编
     */
    private String zipCode;
    /**
     * 地区
     */
    @TableField("area_id")
    private Long areaId;
    /**
     * 会员
     */
    @TableField("member_id")
    private Long memberId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
