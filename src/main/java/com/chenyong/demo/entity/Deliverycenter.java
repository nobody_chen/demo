package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 发货点
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Deliverycenter extends Model<Deliverycenter> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 地址
     */
    private String address;
    /**
     * 地区名称
     */
    private String areaName;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 是否默认
     */
    private Boolean isDefault;
    /**
     * 备注
     */
    private String memo;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 名称
     */
    private String name;
    /**
     * 电话
     */
    private String phone;
    /**
     * 邮编
     */
    private String zipCode;
    /**
     * 地区
     */
    @TableField("area_id")
    private Long areaId;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
