package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 到货通知
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Productnotify extends Model<Productnotify> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * E-mail
     */
    private String email;
    /**
     * 是否已发送
     */
    private Boolean hasSent;
    /**
     * 会员
     */
    @TableField("member_id")
    private Long memberId;
    /**
     * 会员
     */
    @TableField("sku_id")
    private Long skuId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
