package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Users extends Model<Users> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 是否启用
     */
    private Boolean isEnabled;
    /**
     * 是否锁定
     */
    private Boolean isLocked;
    /**
     * 最后登录日期
     */
    private Date lastLoginDate;
    /**
     * 最后登录IP
     */
    private String lastLoginIp;
    /**
     * 锁定日期
     */
    private Date lockDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
