package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单项
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Orderitem extends Model<Orderitem> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 佣金小计
     */
    private BigDecimal commissionTotals;
    /**
     * 是否需要物流
     */
    private Boolean isDelivery;
    /**
     * 名称
     */
    private String name;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 数量
     */
    private Integer quantity;
    /**
     * 已退货数量
     */
    private Integer returnedQuantity;
    /**
     * 已发货数量
     */
    private Integer shippedQuantity;
    /**
     * 编号
     */
    private String sn;
    /**
     * 规格
     */
    private String specifications;
    /**
     * 缩略图
     */
    private String thumbnail;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 重量
     */
    private Integer weight;
    /**
     * 订单
     */
    private Long orders;
    /**
     * SKU
     */
    @TableField("sku_id")
    private Long skuId;
    /**
     * 订单评论状态
     */
    private Boolean reviewStatus;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
