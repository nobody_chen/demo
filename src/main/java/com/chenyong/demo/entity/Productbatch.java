package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品批次
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Productbatch extends Model<Productbatch> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 企业id
     */
    private String enterpriseId;
    /**
     * 企业名称
     */
    private String enterpriseName;
    /**
     * 批次号
     */
    private String batchNo;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 产品Id
     */
    private Long productId;
    /**
     * 批次Id
     */
    private Long batchId;
    /**
     * 生产日期
     */
    private Date productDate;
    /**
     * 产品图片
     */
    private String pictureLocation;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
