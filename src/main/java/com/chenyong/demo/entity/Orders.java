package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Orders extends Model<Orders> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 地址
     */
    private String address;
    /**
     * 订单金额
     */
    private BigDecimal amount;
    /**
     * 已付金额
     */
    private BigDecimal amountPaid;
    /**
     * 地区名称
     */
    private String areaName;
    /**
     * 完成日期
     */
    private Date completeDate;
    /**
     * 收货人
     */
    private String consignee;
    /**
     * 优惠券折扣
     */
    private BigDecimal couponDiscount;
    /**
     * 兑换积分
     */
    private Long exchangePoint;
    /**
     * 过期时间
     */
    private Date expire;
    /**
     * 支付手续费
     */
    private BigDecimal fee;
    /**
     * 运费
     */
    private BigDecimal freight;
    /**
     * 发票
     */
    private String invoiceContent;
    /**
     * 税号
     */
    private String invoiceNo;
    /**
     * 发票
     */
    private String invoiceTitle;
    /**
     * 是否已分配库存
     */
    private Boolean isAllocatedStock;
    /**
     * 是否已兑换积分
     */
    private Boolean isExchangePoint;
    /**
     * 是否已使用优惠码
     */
    private Boolean isUseCouponCode;
    /**
     * 附言
     */
    private String memo;
    /**
     * 调整金额
     */
    private BigDecimal offsetAmount;
    /**
     * 支付方式名称
     */
    private String paymentMethodName;
    /**
     * 支付方式类型
     */
    private Integer paymentMethodType;
    /**
     * 电话
     */
    private String phone;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 促销折扣
     */
    private BigDecimal promotionDiscount;
    /**
     * 促销名称
     */
    private String promotionNames;
    /**
     * 数量
     */
    private Integer quantity;
    /**
     * 退款金额
     */
    private BigDecimal refundAmount;
    /**
     * 已退货数量
     */
    private Integer returnedQuantity;
    /**
     * 赠送积分
     */
    private Long rewardPoint;
    /**
     * 已发货数量
     */
    private Integer shippedQuantity;
    /**
     * 配送方式名称
     */
    private String shippingMethodName;
    /**
     * 编号
     */
    private String sn;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 税金
     */
    private BigDecimal tax;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 重量
     */
    private Integer weight;
    /**
     * 邮编
     */
    private String zipCode;
    /**
     * 地区
     */
    @TableField("area_id")
    private Long areaId;
    /**
     * 优惠码
     */
    @TableField("couponCode_id")
    private Long couponcodeId;
    /**
     * 会员
     */
    @TableField("member_id")
    private Long memberId;
    /**
     * 支付方式
     */
    @TableField("paymentMethod_id")
    private Long paymentmethodId;
    /**
     * 配送方式
     */
    @TableField("shippingMethod_id")
    private Long shippingmethodId;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
