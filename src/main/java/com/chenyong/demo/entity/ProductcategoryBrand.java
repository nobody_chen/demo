package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分类品牌中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("productcategory_brand")
public class ProductcategoryBrand extends Model<ProductcategoryBrand> {

    private static final long serialVersionUID = 1L;

    /**
     * 商品分类
     */
    @TableId("productCategories_id")
    private Long productcategoriesId;
    /**
     * 关联品牌
     */
    @TableField("brands_id")
    private Long brandsId;


    @Override
    protected Serializable pkVal() {
        return this.productcategoriesId;
    }

}
