package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 地区运费配置
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Areafreightconfig extends Model<Areafreightconfig> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 续重价格
     */
    private BigDecimal continuePrice;
    /**
     * 续重量
     */
    private Integer continueWeight;
    /**
     * 首重价格
     */
    private BigDecimal firstPrice;
    /**
     * 首重量
     */
    private Integer firstWeight;
    /**
     * 配送方式
     */
    @TableField("shippingMethod_id")
    private Long shippingmethodId;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;
    /**
     * 地区
     */
    @TableField("area_id")
    private Long areaId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
