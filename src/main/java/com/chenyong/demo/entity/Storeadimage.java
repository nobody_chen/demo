package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 店铺广告图片
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Storeadimage extends Model<Storeadimage> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 图片
     */
    private String image;
    /**
     * 标题
     */
    private String title;
    /**
     * 链接地址
     */
    private String url;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
