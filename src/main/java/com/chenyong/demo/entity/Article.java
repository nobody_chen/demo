package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文章
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Article extends Model<Article> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 作者
     */
    private String author;
    /**
     * 内容
     */
    private String content;
    /**
     * 点击数
     */
    private Long hits;
    /**
     * 是否发布
     */
    private Boolean isPublication;
    /**
     * 是否置顶
     */
    private Boolean isTop;
    /**
     * 页面描述
     */
    private String seoDescription;
    /**
     * 页面关键词
     */
    private String seoKeywords;
    /**
     * 页面标题
     */
    private String seoTitle;
    /**
     * 标题
     */
    private String title;
    /**
     * 文章分类
     */
    @TableField("articleCategory_id")
    private Long articlecategoryId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
