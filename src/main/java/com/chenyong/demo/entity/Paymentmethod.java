package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 支付方式
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Paymentmethod extends Model<Paymentmethod> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 内容
     */
    private String content;
    /**
     * 介绍
     */
    private String description;
    /**
     * 图标
     */
    private String icon;
    /**
     * 方式
     */
    private Integer method;
    /**
     * 名称
     */
    private String name;
    /**
     * 超时时间
     */
    private Integer timeout;
    /**
     * 类型
     */
    private Integer type;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
