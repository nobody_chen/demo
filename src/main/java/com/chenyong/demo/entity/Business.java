package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商家
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Business extends Model<Business> {

    private static final long serialVersionUID = 1L;

    /**
     * 商家注册项值0
     */
    private String attributeValue0;
    /**
     * 商家注册项值1
     */
    private String attributeValue1;
    /**
     * 商家注册项值10
     */
    private String attributeValue10;
    /**
     * 商家注册项值11
     */
    private String attributeValue11;
    /**
     * 商家注册项值12
     */
    private String attributeValue12;
    /**
     * 商家注册项值13
     */
    private String attributeValue13;
    /**
     * 商家注册项值14
     */
    private String attributeValue14;
    /**
     * 商家注册项值15
     */
    private String attributeValue15;
    /**
     * 商家注册项值16
     */
    private String attributeValue16;
    /**
     * 商家注册项值17
     */
    private String attributeValue17;
    /**
     * 商家注册项值18
     */
    private String attributeValue18;
    /**
     * 商家注册项值19
     */
    private String attributeValue19;
    /**
     * 商家注册项值2
     */
    private String attributeValue2;
    /**
     * 商家注册项值3
     */
    private String attributeValue3;
    /**
     * 商家注册项值4
     */
    private String attributeValue4;
    /**
     * 商家注册项值5
     */
    private String attributeValue5;
    /**
     * 商家注册项值6
     */
    private String attributeValue6;
    /**
     * 商家注册项值7
     */
    private String attributeValue7;
    /**
     * 商家注册项值8
     */
    private String attributeValue8;
    /**
     * 商家注册项值9
     */
    private String attributeValue9;
    /**
     * 余额
     */
    private BigDecimal balance;
    /**
     * 银行账号
     */
    private String bankAccount;
    /**
     * 银行开户名
     */
    private String bankName;
    /**
     * E-mail
     */
    private String email;
    /**
     * 加密密码
     */
    private String encodedPassword;
    /**
     * 冻结金额
     */
    private BigDecimal frozenFund;
    /**
     * 法人代表身份证
     */
    private String idCard;
    /**
     * 法人代表身份证图片
     */
    private String idCardImage;
    /**
     * 纳税人识别号
     */
    private String identificationNumber;
    /**
     * 法人代表姓名
     */
    private String legalPerson;
    /**
     * 营业执照号图片
     */
    private String licenseImage;
    /**
     * 营业执照号
     */
    private String licenseNumber;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 名称
     */
    private String name;
    /**
     * 组织机构代码
     */
    private String organizationCode;
    /**
     * 组织机构代码证图片
     */
    private String organizationImage;
    /**
     * 电话
     */
    private String phone;
    /**
     * 安全密匙
     */
    private Date safeKeyExpire;
    /**
     * 安全密匙
     */
    private String safeKeyValue;
    /**
     * 税务登记证图片
     */
    private String taxImage;
    /**
     * 用户名
     */
    private String username;
    /**
     * ID
     */
    private Long id;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
