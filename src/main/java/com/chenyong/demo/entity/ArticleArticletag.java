package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文章文章标签中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("article_articletag")
public class ArticleArticletag extends Model<ArticleArticletag> {

    private static final long serialVersionUID = 1L;

    /**
     * 文章
     */
    @TableId("articles_id")
    private Long articlesId;
    /**
     * 文章标签
     */
    @TableField("articleTags_id")
    private Long articletagsId;


    @Override
    protected Serializable pkVal() {
        return this.articlesId;
    }

}
