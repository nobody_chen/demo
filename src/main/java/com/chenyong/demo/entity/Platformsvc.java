package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 平台服务
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Platformsvc extends Model<Platformsvc> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
