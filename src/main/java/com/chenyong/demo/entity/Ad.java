package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 广告
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Ad extends Model<Ad> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 起始日期
     */
    private Date beginDate;
    /**
     * 内容
     */
    private String content;
    /**
     * 结束日期
     */
    private Date endDate;
    /**
     * 路径
     */
    private String path;
    /**
     * 标题
     */
    private String title;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 链接地址
     */
    private String url;
    /**
     * 广告位
     */
    @TableField("adPosition_id")
    private Long adpositionId;
    /**
     * 域名id
     */
    @TableField("host_id")
    private Long hostId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
