package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品分类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Productcategory extends Model<Productcategory> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 普通店铺分佣比例
     */
    private Double generalRate;
    /**
     * 层级
     */
    private Integer grade;
    /**
     * 名称
     */
    private String name;
    /**
     * 自营店铺分佣比例
     */
    private Double selfRate;
    /**
     * 页面描述
     */
    private String seoDescription;
    /**
     * 页面关键词
     */
    private String seoKeywords;
    /**
     * 页面标题
     */
    private String seoTitle;
    /**
     * 树路径
     */
    private String treePath;
    /**
     * 上级分类
     */
    @TableField("parent_id")
    private Long parentId;
    /**
     * 域名id
     */
    @TableField("host_id")
    private Long hostId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
