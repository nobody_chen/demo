package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * ID管理表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Idgenerator extends Model<Idgenerator> {

    private static final long serialVersionUID = 1L;

    /**
     * 序列名
     */
    @TableId("sequence_name")
    private String sequenceName;
    /**
     * 序列值
     */
    @TableField("next_val")
    private Long nextVal;


    @Override
    protected Serializable pkVal() {
        return this.sequenceName;
    }

}
