package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 积分记录
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Pointlog extends Model<Pointlog> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 当前积分
     */
    private Long balance;
    /**
     * 获取积分
     */
    private Long credit;
    /**
     * 扣除积分
     */
    private Long debit;
    /**
     * 备注
     */
    private String memo;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 会员
     */
    @TableField("member_id")
    private Long memberId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
