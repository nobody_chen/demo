package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品商品标签中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("product_producttag")
public class ProductProducttag extends Model<ProductProducttag> {

    private static final long serialVersionUID = 1L;

    /**
     * 商品
     */
    @TableId("products_id")
    private Long productsId;
    /**
     * 商品标签
     */
    @TableField("productTags_id")
    private Long producttagsId;


    @Override
    protected Serializable pkVal() {
        return this.productsId;
    }

}
