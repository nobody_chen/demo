package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 快递单模板
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Deliverytemplate extends Model<Deliverytemplate> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 背景图
     */
    private String background;
    /**
     * 内容
     */
    private String content;
    /**
     * 高度
     */
    private Integer height;
    /**
     * 是否默认
     */
    private Boolean isDefault;
    /**
     * 备注
     */
    private String memo;
    /**
     * 名称
     */
    private String name;
    /**
     * 偏移量X
     */
    private Integer offsetX;
    /**
     * 偏移量Y
     */
    private Integer offsetY;
    /**
     * 宽度
     */
    private Integer width;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
