package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 管理员
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Admin extends Model<Admin> {

    private static final long serialVersionUID = 1L;

    /**
     * 部门
     */
    private String department;
    /**
     * E-mail
     */
    private String email;
    /**
     * 加密密码
     */
    private String encodedPassword;
    /**
     * 姓名
     */
    private String name;
    /**
     * 用户名
     */
    private String username;
    /**
     * ID
     */
    private Long id;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
