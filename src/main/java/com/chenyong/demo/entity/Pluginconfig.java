package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 插件配置
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Pluginconfig extends Model<Pluginconfig> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 属性
     */
    private String attributes;
    /**
     * 是否启用
     */
    private Boolean isEnabled;
    /**
     * 插件ID
     */
    private String pluginId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
