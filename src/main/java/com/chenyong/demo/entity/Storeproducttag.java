package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Storeproducttag extends Model<Storeproducttag> {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date createdDate;
    private Date lastModifiedDate;
    private Long version;
    private Integer orders;
    private String icon;
    private Boolean isEnabled;
    private String memo;
    private String name;
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
