package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 发货项
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Ordershippingitem extends Model<Ordershippingitem> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 是否需要物流
     */
    private Boolean isDelivery;
    /**
     * SKU名称
     */
    private String name;
    /**
     * 数量
     */
    private Integer quantity;
    /**
     * SKU编号
     */
    private String sn;
    /**
     * 规格
     */
    private String specifications;
    /**
     * 订单发货
     */
    @TableField("orderShipping_id")
    private Long ordershippingId;
    /**
     * SKU
     */
    @TableField("sku_id")
    private Long skuId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
