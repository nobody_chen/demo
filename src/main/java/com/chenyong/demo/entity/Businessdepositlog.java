package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商家预存款记录
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Businessdepositlog extends Model<Businessdepositlog> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 当前余额
     */
    private BigDecimal balance;
    /**
     * 收入金额
     */
    private BigDecimal credit;
    /**
     * 支出金额
     */
    private BigDecimal debit;
    /**
     * 备注
     */
    private String memo;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 商家
     */
    @TableField("business_id")
    private Long businessId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
