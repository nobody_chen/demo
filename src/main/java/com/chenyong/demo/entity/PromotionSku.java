package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 促销SKU中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("promotion_sku")
public class PromotionSku extends Model<PromotionSku> {

    private static final long serialVersionUID = 1L;

    /**
     * 赠品促销
     */
    @TableId("giftPromotions_id")
    private Long giftpromotionsId;
    /**
     * 赠品
     */
    @TableField("gifts_id")
    private Long giftsId;


    @Override
    protected Serializable pkVal() {
        return this.giftpromotionsId;
    }

}
