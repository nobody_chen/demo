package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 促销会员等级中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("promotion_memberrank")
public class PromotionMemberrank extends Model<PromotionMemberrank> {

    private static final long serialVersionUID = 1L;

    /**
     * 促销
     */
    @TableId("promotions_id")
    private Long promotionsId;
    /**
     * 允许参加会员等级
     */
    @TableField("memberRanks_id")
    private Long memberranksId;


    @Override
    protected Serializable pkVal() {
        return this.promotionsId;
    }

}
