package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单退款
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Orderrefunds extends Model<Orderrefunds> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 退款账号
     */
    private String account;
    /**
     * 退款金额
     */
    private BigDecimal amount;
    /**
     * 退款银行
     */
    private String bank;
    /**
     * 备注
     */
    private String memo;
    /**
     * 方式
     */
    private Integer method;
    /**
     * 收款人
     */
    private String payee;
    /**
     * 支付方式
     */
    private String paymentMethod;
    /**
     * 编号
     */
    private String sn;
    /**
     * 订单
     */
    private Long orders;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
