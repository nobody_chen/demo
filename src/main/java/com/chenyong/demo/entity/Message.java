package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Message extends Model<Message> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 内容
     */
    private String content;
    /**
     * ip
     */
    private String ip;
    /**
     * 是否为草稿
     */
    private Boolean isDraft;
    /**
     * 收件人删除
     */
    private Boolean receiverDelete;
    /**
     * 收件人已读
     */
    private Boolean receiverRead;
    /**
     * 发件人删除
     */
    private Boolean senderDelete;
    /**
     * 发件人已读
     */
    private Boolean senderRead;
    /**
     * 标题
     */
    private String title;
    /**
     * 原消息
     */
    @TableField("forMessage_id")
    private Long formessageId;
    /**
     * 收件人
     */
    @TableField("receiver_id")
    private Long receiverId;
    /**
     * 发件人
     */
    @TableField("sender_id")
    private Long senderId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
