package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单优惠券中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("orders_coupon")
public class OrdersCoupon extends Model<OrdersCoupon> {

    private static final long serialVersionUID = 1L;

    /**
     * 订单
     */
    @TableField("orders_id")
    private Long ordersId;
    /**
     * 赠送优惠券
     */
    @TableField("coupons_id")
    private Long couponsId;


    @Override
    protected Serializable pkVal() {
        return this.ordersId;
    }

}
