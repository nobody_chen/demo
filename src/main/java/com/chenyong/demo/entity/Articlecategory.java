package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 文章分类
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Articlecategory extends Model<Articlecategory> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 排序
     */
    private Integer orders;
    /**
     * 层级
     */
    private Integer grade;
    /**
     * 名称
     */
    private String name;
    /**
     * 页面描述
     */
    private String seoDescription;
    /**
     * 页面关键词
     */
    private String seoKeywords;
    /**
     * 页面标题
     */
    private String seoTitle;
    /**
     * 树路径
     */
    private String treePath;
    /**
     * 上级分类
     */
    @TableField("parent_id")
    private Long parentId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
