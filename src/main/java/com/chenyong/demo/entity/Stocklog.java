package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 库存记录
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Stocklog extends Model<Stocklog> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 入库数量
     */
    private Integer inQuantity;
    /**
     * 备注
     */
    private String memo;
    /**
     * 出库数量
     */
    private Integer outQuantity;
    /**
     * 当前库存
     */
    private Integer stock;
    /**
     * 类型
     */
    private Integer type;
    /**
     * SKU
     */
    @TableField("sku_id")
    private Long skuId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
