package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 店铺
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Store extends Model<Store> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 地址
     */
    private String address;
    /**
     * 已付保证金
     */
    private BigDecimal bailPaid;
    /**
     * 折扣促销到期日期
     */
    private Date discountPromotionEndDate;
    /**
     * E-mail
     */
    private String email;
    /**
     * 到期日期
     */
    private Date endDate;
    /**
     * 满减促销到期日期
     */
    private Date fullReductionPromotionEndDate;
    /**
     * 简介
     */
    private String introduction;
    /**
     * 是否启用
     */
    private Boolean isEnabled;
    /**
     * 搜索关键词
     */
    private String keyword;
    /**
     * logo
     */
    private String logo;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 名称
     */
    private String name;
    /**
     * 电话
     */
    private String phone;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 邮编
     */
    private String zipCode;
    /**
     * 商家
     */
    @TableField("business_id")
    private Long businessId;
    /**
     * 店铺分类
     */
    @TableField("storeCategory_id")
    private Long storecategoryId;
    /**
     * 店铺等级
     */
    @TableField("storeRank_id")
    private Long storerankId;
    /**
     * 域名id
     */
    @TableField("host_id")
    private Long hostId;
    /**
     * 企业id
     */
    private String enterpriseId;
    /**
     * 企业名称
     */
    private String enterpriseName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
