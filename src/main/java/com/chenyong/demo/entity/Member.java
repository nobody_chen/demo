package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 会员
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Member extends Model<Member> {

    private static final long serialVersionUID = 1L;

    /**
     * 地址
     */
    private String address;
    /**
     * 消费金额
     */
    private BigDecimal amount;
    /**
     * 会员注册项值0
     */
    private String attributeValue0;
    /**
     * 会员注册项值1
     */
    private String attributeValue1;
    /**
     * 会员注册项值2
     */
    private String attributeValue2;
    /**
     * 会员注册项值3
     */
    private String attributeValue3;
    /**
     * 会员注册项值4
     */
    private String attributeValue4;
    /**
     * 会员注册项值5
     */
    private String attributeValue5;
    /**
     * 会员注册项值6
     */
    private String attributeValue6;
    /**
     * 会员注册项值7
     */
    private String attributeValue7;
    /**
     * 会员注册项值8
     */
    private String attributeValue8;
    /**
     * 会员注册项值9
     */
    private String attributeValue9;
    /**
     * 余额
     */
    private BigDecimal balance;
    /**
     * 出生日期
     */
    private Date birth;
    /**
     * E-mail
     */
    private String email;
    /**
     * 加密密码
     */
    private String encodedPassword;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 手机
     */
    private String mobile;
    /**
     * 姓名
     */
    private String name;
    /**
     * 电话
     */
    private String phone;
    /**
     * 积分
     */
    private Long point;
    /**
     * 安全密匙
     */
    private Date safeKeyExpire;
    /**
     * 安全密匙
     */
    private String safeKeyValue;
    /**
     * 用户名
     */
    private String username;
    /**
     * 邮编
     */
    private String zipCode;
    /**
     * ID
     */
    private Long id;
    /**
     * 地区
     */
    @TableField("area_id")
    private Long areaId;
    /**
     * 会员等级
     */
    @TableField("memberRank_id")
    private Long memberrankId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
