package com.chenyong.demo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 优惠券
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Coupon extends Model<Coupon> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 使用起始日期
     */
    private Date beginDate;
    /**
     * 使用结束日期
     */
    private Date endDate;
    /**
     * 介绍
     */
    private String introduction;
    /**
     * 是否启用
     */
    private Boolean isEnabled;
    /**
     * 是否允许积分兑换
     */
    private Boolean isExchange;
    /**
     * 最大商品价格
     */
    private BigDecimal maximumPrice;
    /**
     * 最大商品数量
     */
    private Integer maximumQuantity;
    /**
     * 最小商品价格
     */
    private BigDecimal minimumPrice;
    /**
     * 最小商品数量
     */
    private Integer minimumQuantity;
    /**
     * 名称
     */
    private String name;
    /**
     * 积分兑换数
     */
    private Long point;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 价格运算表达式
     */
    private String priceExpression;
    /**
     * 店铺
     */
    @TableField("store_id")
    private Long storeId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
