package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Brand extends Model<Brand> {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Date createdDate;
    private Date lastModifiedDate;
    private Long version;
    private Integer orders;
    private String introduction;
    private String logo;
    private String name;
    private Integer type;
    private String url;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
