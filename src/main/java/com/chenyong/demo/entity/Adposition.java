package com.chenyong.demo.entity;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 广告位
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Adposition extends Model<Adposition> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private Long id;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 描述
     */
    private String description;
    /**
     * 高度
     */
    private Integer height;
    /**
     * 名称
     */
    private String name;
    /**
     * 模板
     */
    private String template;
    /**
     * 宽度
     */
    private Integer width;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
