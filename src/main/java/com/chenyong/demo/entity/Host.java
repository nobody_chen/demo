package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 域名配置
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
public class Host extends Model<Host> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 域名
     */
    private String host;
    /**
     * 域名描述
     */
    private String hostDesc;
    /**
     * 商城首页模板
     */
    private String shopIndexTemplate;
    /**
     * 商家首页模板
     */
    private String businessIndexTemplate;
    /**
     * 创建日期
     */
    private Date createdDate;
    /**
     * 最后修改日期
     */
    private Date lastModifiedDate;
    /**
     * 版本
     */
    private Long version;
    /**
     * 域名自定义的logo，通常为建瓯、明溪政府指定的logo
     */
    private String logo;
    /**
     * 网站名称
     */
    private String siteName;
    /**
     * 热门搜索
     */
    private String hotSearch;
    /**
     * 联系地址
     */
    private String address;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 邮政编码
     */
    private String zipCode;
    /**
     * 备案编号
     */
    private String certtext;
    /**
     * Copyright
     */
    private String copyright;
    /**
     * ess登录地址
     */
    private String essLoginUrl;
    private Boolean isFromOldSystem;
    private String appQRCode;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
