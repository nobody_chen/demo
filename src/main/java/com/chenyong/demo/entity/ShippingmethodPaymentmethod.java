package com.chenyong.demo.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.Version;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 配送方式支付方式中间表
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
@Data
@Accessors(chain = true)
@TableName("shippingmethod_paymentmethod")
public class ShippingmethodPaymentmethod extends Model<ShippingmethodPaymentmethod> {

    private static final long serialVersionUID = 1L;

    /**
     * 配送方式
     */
    @TableId("shippingMethods_id")
    private Long shippingmethodsId;
    /**
     * 支持支付方式
     */
    @TableField("paymentMethods_id")
    private Long paymentmethodsId;


    @Override
    protected Serializable pkVal() {
        return this.shippingmethodsId;
    }

}
