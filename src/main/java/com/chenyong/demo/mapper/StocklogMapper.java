package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Stocklog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 库存记录 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StocklogMapper extends BaseMapper<Stocklog> {

}
