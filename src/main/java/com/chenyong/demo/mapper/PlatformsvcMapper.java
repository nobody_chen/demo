package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Platformsvc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 平台服务 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PlatformsvcMapper extends BaseMapper<Platformsvc> {

}
