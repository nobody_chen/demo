package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Articlecategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 文章分类 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ArticlecategoryMapper extends BaseMapper<Articlecategory> {

}
