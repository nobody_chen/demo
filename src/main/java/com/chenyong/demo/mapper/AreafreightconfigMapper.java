package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Areafreightconfig;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 地区运费配置 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface AreafreightconfigMapper extends BaseMapper<Areafreightconfig> {

}
