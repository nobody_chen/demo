package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Articletag;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 文章标签 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ArticletagMapper extends BaseMapper<Articletag> {

}
