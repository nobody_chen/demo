package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Pluginconfig;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 插件配置 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PluginconfigMapper extends BaseMapper<Pluginconfig> {

}
