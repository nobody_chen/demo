package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Orderitem;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单项 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface OrderitemMapper extends BaseMapper<Orderitem> {

}
