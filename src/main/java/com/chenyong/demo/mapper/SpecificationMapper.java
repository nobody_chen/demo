package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Specification;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 规格 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface SpecificationMapper extends BaseMapper<Specification> {

}
