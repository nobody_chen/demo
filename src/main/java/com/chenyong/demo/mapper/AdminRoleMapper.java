package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.AdminRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 管理员角色中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface AdminRoleMapper extends BaseMapper<AdminRole> {

}
