package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Deliverytemplate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 快递单模板 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface DeliverytemplateMapper extends BaseMapper<Deliverytemplate> {

}
