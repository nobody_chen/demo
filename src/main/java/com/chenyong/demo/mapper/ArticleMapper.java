package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Article;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 文章 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ArticleMapper extends BaseMapper<Article> {

}
