package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Promotionpluginsvc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 促销插件服务 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PromotionpluginsvcMapper extends BaseMapper<Promotionpluginsvc> {

}
