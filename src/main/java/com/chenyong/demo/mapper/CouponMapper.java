package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Coupon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优惠券 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface CouponMapper extends BaseMapper<Coupon> {

}
