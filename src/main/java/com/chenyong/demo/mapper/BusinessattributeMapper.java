package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Businessattribute;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商家注册项 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface BusinessattributeMapper extends BaseMapper<Businessattribute> {

}
