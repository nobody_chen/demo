package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Productbatch;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品批次 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ProductbatchMapper extends BaseMapper<Productbatch> {

}
