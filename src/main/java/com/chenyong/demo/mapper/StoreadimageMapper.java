package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Storeadimage;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 店铺广告图片 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StoreadimageMapper extends BaseMapper<Storeadimage> {

}
