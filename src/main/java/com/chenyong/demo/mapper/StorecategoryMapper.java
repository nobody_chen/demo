package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Storecategory;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 店铺分类 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StorecategoryMapper extends BaseMapper<Storecategory> {

}
