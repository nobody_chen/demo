package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Ordershipping;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单发货 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface OrdershippingMapper extends BaseMapper<Ordershipping> {

}
