package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Adposition;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 广告位 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface AdpositionMapper extends BaseMapper<Adposition> {

}
