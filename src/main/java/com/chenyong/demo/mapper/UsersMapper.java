package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Users;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface UsersMapper extends BaseMapper<Users> {

}
