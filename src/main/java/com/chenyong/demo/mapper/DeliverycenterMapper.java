package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Deliverycenter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 发货点 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface DeliverycenterMapper extends BaseMapper<Deliverycenter> {

}
