package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.ShippingmethodPaymentmethod;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 配送方式支付方式中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ShippingmethodPaymentmethodMapper extends BaseMapper<ShippingmethodPaymentmethod> {

}
