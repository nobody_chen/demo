package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Orders;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
