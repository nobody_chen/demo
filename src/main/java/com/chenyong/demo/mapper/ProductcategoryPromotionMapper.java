package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.ProductcategoryPromotion;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品分类促销中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ProductcategoryPromotionMapper extends BaseMapper<ProductcategoryPromotion> {

}
