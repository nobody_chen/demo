package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Review;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ReviewMapper extends BaseMapper<Review> {

}
