package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Navigation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 导航 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface NavigationMapper extends BaseMapper<Navigation> {

}
