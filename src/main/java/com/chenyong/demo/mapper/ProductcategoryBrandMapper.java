package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.ProductcategoryBrand;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品分类品牌中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ProductcategoryBrandMapper extends BaseMapper<ProductcategoryBrand> {

}
