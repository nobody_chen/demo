package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Messageconfig;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 消息配置 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface MessageconfigMapper extends BaseMapper<Messageconfig> {

}
