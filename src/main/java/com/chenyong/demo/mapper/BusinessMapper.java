package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Business;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商家 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface BusinessMapper extends BaseMapper<Business> {

}
