package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Parameter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 参数 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ParameterMapper extends BaseMapper<Parameter> {

}
