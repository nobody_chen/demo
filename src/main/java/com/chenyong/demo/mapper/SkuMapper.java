package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Sku;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * SKU Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface SkuMapper extends BaseMapper<Sku> {

}
