package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.PromotionSku;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 促销SKU中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PromotionSkuMapper extends BaseMapper<PromotionSku> {

}
