package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Pointlog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 积分记录 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PointlogMapper extends BaseMapper<Pointlog> {

}
