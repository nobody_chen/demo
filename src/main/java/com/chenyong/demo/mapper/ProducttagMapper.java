package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Producttag;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品标签 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ProducttagMapper extends BaseMapper<Producttag> {

}
