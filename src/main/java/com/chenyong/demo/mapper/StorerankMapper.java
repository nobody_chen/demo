package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Storerank;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 店铺等级 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StorerankMapper extends BaseMapper<Storerank> {

}
