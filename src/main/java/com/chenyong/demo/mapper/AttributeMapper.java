package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Attribute;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 属性 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface AttributeMapper extends BaseMapper<Attribute> {

}
