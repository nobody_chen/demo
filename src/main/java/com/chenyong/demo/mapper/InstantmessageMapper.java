package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Instantmessage;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 即时通讯 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface InstantmessageMapper extends BaseMapper<Instantmessage> {

}
