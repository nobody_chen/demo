package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Consultation;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 咨询 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ConsultationMapper extends BaseMapper<Consultation> {

}
