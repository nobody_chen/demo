package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Orderreturnsitem;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 退货项 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface OrderreturnsitemMapper extends BaseMapper<Orderreturnsitem> {

}
