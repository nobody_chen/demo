package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Area;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 地区 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface AreaMapper extends BaseMapper<Area> {

}
