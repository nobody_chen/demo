package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Friendlink;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 友情链接 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface FriendlinkMapper extends BaseMapper<Friendlink> {

}
