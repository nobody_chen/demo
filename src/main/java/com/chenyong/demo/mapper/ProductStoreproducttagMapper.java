package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.ProductStoreproducttag;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品店铺商品标签中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ProductStoreproducttagMapper extends BaseMapper<ProductStoreproducttag> {

}
