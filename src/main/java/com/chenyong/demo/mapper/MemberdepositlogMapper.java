package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Memberdepositlog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员预存款记录 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface MemberdepositlogMapper extends BaseMapper<Memberdepositlog> {

}
