package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Storefavorite;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 店铺收藏 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StorefavoriteMapper extends BaseMapper<Storefavorite> {

}
