package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Defaultfreightconfig;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 默认运费配置 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface DefaultfreightconfigMapper extends BaseMapper<Defaultfreightconfig> {

}
