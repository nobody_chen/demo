package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Admin;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 管理员 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
