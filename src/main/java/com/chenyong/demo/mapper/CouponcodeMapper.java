package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Couponcode;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 优惠码 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface CouponcodeMapper extends BaseMapper<Couponcode> {

}
