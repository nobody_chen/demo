package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Businessdepositlog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商家预存款记录 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface BusinessdepositlogMapper extends BaseMapper<Businessdepositlog> {

}
