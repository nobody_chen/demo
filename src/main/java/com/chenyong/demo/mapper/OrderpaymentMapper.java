package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Orderpayment;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单支付 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface OrderpaymentMapper extends BaseMapper<Orderpayment> {

}
