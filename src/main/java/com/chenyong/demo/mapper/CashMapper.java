package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Cash;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 提现 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface CashMapper extends BaseMapper<Cash> {

}
