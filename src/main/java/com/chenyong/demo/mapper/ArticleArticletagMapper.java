package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.ArticleArticletag;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 文章文章标签中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface ArticleArticletagMapper extends BaseMapper<ArticleArticletag> {

}
