package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Svc;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 服务 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface SvcMapper extends BaseMapper<Svc> {

}
