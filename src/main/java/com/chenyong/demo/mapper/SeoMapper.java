package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Seo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * SEO设置 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface SeoMapper extends BaseMapper<Seo> {

}
