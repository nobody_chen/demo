package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Deliverycorp;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 物流公司 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface DeliverycorpMapper extends BaseMapper<Deliverycorp> {

}
