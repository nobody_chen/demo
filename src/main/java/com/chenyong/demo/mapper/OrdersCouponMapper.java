package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.OrdersCoupon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单优惠券中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface OrdersCouponMapper extends BaseMapper<OrdersCoupon> {

}
