package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Idgenerator;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * ID管理表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface IdgeneratorMapper extends BaseMapper<Idgenerator> {

}
