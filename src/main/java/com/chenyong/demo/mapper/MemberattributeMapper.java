package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Memberattribute;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员注册项 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface MemberattributeMapper extends BaseMapper<Memberattribute> {

}
