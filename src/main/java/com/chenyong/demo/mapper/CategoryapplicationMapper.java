package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Categoryapplication;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 经营分类申请 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface CategoryapplicationMapper extends BaseMapper<Categoryapplication> {

}
