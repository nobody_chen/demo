package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Socialuser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 社会化用户 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface SocialuserMapper extends BaseMapper<Socialuser> {

}
