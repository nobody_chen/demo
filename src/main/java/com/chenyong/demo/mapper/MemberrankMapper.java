package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Memberrank;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员等级 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface MemberrankMapper extends BaseMapper<Memberrank> {

}
