package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Statistic;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 统计 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StatisticMapper extends BaseMapper<Statistic> {

}
