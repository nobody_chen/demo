package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Paymentmethod;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 支付方式 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PaymentmethodMapper extends BaseMapper<Paymentmethod> {

}
