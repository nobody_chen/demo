package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Message;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 消息 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface MessageMapper extends BaseMapper<Message> {

}
