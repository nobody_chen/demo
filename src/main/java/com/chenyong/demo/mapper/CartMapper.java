package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Cart;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 购物车 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface CartMapper extends BaseMapper<Cart> {

}
