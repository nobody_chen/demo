package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.PromotionCoupon;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 促销优惠券中间表 Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface PromotionCouponMapper extends BaseMapper<PromotionCoupon> {

}
