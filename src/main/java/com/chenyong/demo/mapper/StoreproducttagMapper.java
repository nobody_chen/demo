package com.chenyong.demo.mapper;

import com.chenyong.demo.entity.Storeproducttag;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 陈勇
 * @since 2018-04-28
 */
public interface StoreproducttagMapper extends BaseMapper<Storeproducttag> {

}
